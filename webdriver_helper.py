from selenium import webdriver
from selenium.webdriver.common.by import By
import selenium.webdriver.support.expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import TimeoutException
timeout_s=5

def check_if_xpath_exists(driver, xpath):
    elems = driver.find_elements_by_xpath(xpath)
    if len(elems) == 0:
        return False
    return elems[0]


def check_if_id_exists(driver, id):
    elems = driver.find_elements_by_id(id)
    if len(elems) == 0:
        return False
    return elems[0]

def check_if_id_exists(driver, id):
    elems = driver.find_elements_by_id(id)
    if len(elems) == 0:
        return False
    return elems[0]


def wait_for_id(driver, id, timeout=timeout_s):
    return wait_for_by(driver, By.ID, id, timeout)
def wait_for_xpath(driver, id, timeout=timeout_s):
    return wait_for_by(driver, By.XPATH, id, timeout)


def wait_for_by(driver, by, var, timeout=timeout_s):
    try:
        e = WebDriverWait(driver, float(timeout)).until(EC.presence_of_element_located((by, var)))
        return e
    except TimeoutException:
        return False
    except Exception:
        return False