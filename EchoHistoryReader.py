import time
import logging
import sys
import pickle

import selenium
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
import selenium.webdriver.support.expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import TimeoutException
import code

from webdriver_helper import check_if_id_exists, check_if_xpath_exists, wait_for_id, wait_for_xpath
from AmazonConstants import *

logger = logging.getLogger(__name__)


class EchoHistoryReader:


    def __init__(self, driver: webdriver):
        #chrome_options = Options()
        # chrome_options.add_argument("--headless")
        #self.driver = webdriver.Chrome(options=chrome_options)
        self.driver = driver
        self.driver.get(URL_ALEXA_HISTORY)
        if not self.amazon_login():
            logger.critical("login failed, exiting...")
            raise Exception("Login failed!!!")
        time.sleep(4) # some loading time...
        self.driver.get(URL_ALEXA_HISTORY)
        logger.debug("login ok!")

    def _amazon_check_login_state(self, timeout=5):
        return wait_for_id(self.driver, ID_APP, timeout)

    def load_cookies(self):
        logger.info("Loading Cookies")
        try:
            cookie_jar = pickle.load(open("cookie.dat", 'rb'))
            self.driver.get(URL_HOME)
            for cookie in cookie_jar:
                self.driver.add_cookie(cookie)
        except Exception as e:
            logger.exception("Error adding Cookies: %s" % e)
            return 0
        else:
            logger.log(logging.INFO, 'added cookies ok, reloading domain')
            self.driver.get(URL_HOME)
            return 1

    def save_cookies(self):
        logger.info("Saving Cookies")
        try:
            pickle.dump(self.driver.get_cookies(), open("cookie.dat", "wb"))
        except pickle.PickleError as e:
            logger.warning("PickleError: %s" % e)
        except selenium.common.exceptions.WebDriverException as e:
            logger.warning("Webdriver exception: %s" % e)
        except Exception as e:
            logger.warning("Unknown exception in save_cookies: %s" % e)

    def amazon_manual_login(self, save=True):
        logger.error("Manual login required!")
        try:
            element = WebDriverWait(self.driver, 120).until(EC.presence_of_element_located((By.ID, "d-app")))
        except TimeoutException:
            logger.critical("Manual login exited after 5 Minutes")
            self.myexit()
        else:
            logger.info("ok?")
            if save:
                self.save_cookies()
            return True

    def amazon_login(self):
        self.load_cookies()
        if self._amazon_check_login_state(5):
            return True

        self.amazon_manual_login()
        if self._amazon_check_login_state(10):
            return True
        return False

    def read_history(self):
        h_list = []
        try:
            history_lists = self.driver.find_elements_by_class_name(CLASS_HISTORY_LIST)  # most recent one is first
            history_list_items = history_lists[0].find_elements_by_class_name(CLASS_HISTORY_ITEM)
            for itm in history_list_items:
                itm_text_element = itm.find_element_by_class_name("d-dialog-title")
                text = itm_text_element.get_attribute('innerText')
                h_list.append(text)
        except:
            logger.error("error, couldn't find history-list ;(")
        return h_list






















