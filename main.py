import time
import logging
import sys
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import *
import os

from EchoHistoryReader import EchoHistoryReader

# Setting up Logger:
rootlogger = logging.getLogger()
rootlogger.setLevel(logging.NOTSET)
logger = logging.getLogger(__name__)
ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.INFO)  # logging.WARNING
formatter = logging.Formatter('%(asctime)s  %(name)-20s  %(threadName)-20s %(funcName)-20s %(levelname)8s: %(message)s')
ch.setFormatter(formatter)
rootlogger.addHandler(ch)

driver: webdriver


def myexit():
    try:
        driver.close()
    except:
        pass
    exit()


def main():
    global driver
    chrome_options = Options()
    #chrome_options.add_argument("--headless")
    logger.info(os.getcwd() + "\\chromedriver.exe")
    driver = webdriver.Chrome(options=chrome_options, executable_path=os.getcwd() + "\\chromedriver.exe")

    ehr: EchoHistoryReader
    try:
        ehr = EchoHistoryReader(driver)
        logger.info("login ok!")
    except Exception as e:
        logger.fatal(e)
        myexit()

    try:
        old_list = ehr.read_history()
        while True:
            time.sleep(1)
            new_list = ehr.read_history()
            if len(old_list) != len(new_list):
                logger.info("New entry: %s" % new_list[0])
                old_list = new_list

        #variables = globals().copy()
        #variables.update(locals())
        #shell = code.InteractiveConsole(variables)
        #shell.interact()
    except KeyboardInterrupt:
        myexit()
    except Exception as e:
        print(e)
        myexit()
    myexit()





if __name__ == "__main__":
    logger.info("Starting Amazon Alexa history reader.")
    main()
