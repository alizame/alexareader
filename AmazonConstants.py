# Defining Constants
URL_ALEXA_HISTORY = "https://alexa.amazon.de/spa/index.html#settings/dialogs"
URL_HOME = URL_ALEXA_HISTORY
CLASS_AUTH_SECTION = "a-section auth-pagelet-mobile-container"
CLASS_HISTORY_LIST = "d-list-highlight-hover"
CLASS_HISTORY_ITEM = "d-dialog-item"
CLASS_HISTORY_ITEM_TEXT = "dd-title d-dialog-title"
ID_APP = "d-app"